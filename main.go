package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"golang.org/x/net/html"
)

type Request struct {
	Url string `json:"url"`
}

type OgData struct {
	Title         string
	Description   string
	Price         int
	PriceCurrency int
	Image         string
}

func main() {
	http.HandleFunc("/", parseLink)
	log.Fatal(http.ListenAndServe("0.0.0.0:8080", nil))
}

func parseLink(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Parse link started")
	if r.Method != http.MethodPost {
		fmt.Println("wrong method")
		http.Error(w, "Only POST requests are allowed", http.StatusMethodNotAllowed)
		return
	}
	fmt.Println("continue")

	// get the link
	var requestData Request
	body, err := io.ReadAll(r.Body)
	if err != nil {
		panic("readall panic")
	}
	fmt.Println(string(body))
	err = json.Unmarshal(body, &requestData)
	if err != nil {
		panic("Unmarshal panic")
	}
	//decoder := json.NewDecoder(r.Body)
	//if err := decoder.Decode(&requestData); err != nil {
	//	panic("Failed to decode JSON")
	//}
	fmt.Println(requestData.Url)
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Println("Ошибка при закрытии Body.Close:", err)
		}
	}(r.Body)

	node := getNode(requestData.Url)

	ogData := getOgData(node)
	fmt.Println(ogData)
	responseJSON, err := json.Marshal(ogData)
	if err != nil {
		fmt.Println("Ошибка при маршалинге JSON:", err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(responseJSON)
	if err != nil {
		fmt.Println("Ошибка отправки ответа:", err)
	}
}

func getNode(url string) *html.Node {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal("Ошибка при получении страницы:", err)
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatal("Ошибка при закрытии:", err)
		}
	}(resp.Body)

	doc, err := html.Parse(resp.Body)
	if err != nil {
		log.Fatal("Ошибка при парсинге HTML:", err)
	}

	return doc
}

func getOgData(node *html.Node) OgData {
	metaData := make(map[string]string)
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "meta" {
			var key, value string
			for _, attr := range n.Attr {
				if attr.Key == "name" || attr.Key == "property" {
					key = attr.Val
				} else if attr.Key == "content" {
					value = attr.Val
				}
			}
			if key != "" && value != "" && strings.Contains(key, "og:") {
				metaData[key] = value
			}
		}

		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}

	f(node)

	return OgData{
		Title:       metaData["og:title"],
		Description: metaData["og:description"],
		Image:       metaData["og:image"],
	}
}
